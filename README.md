Upadated version of Corecalc to be used as Benchmarks for https://gitlab.com/ImDreamer/benchmarks-p10.

This project has stripped all GUI and changed the project target to be .net 6 but otherwise is the same as hosted at:
https://www.itu.dk/people/sestoft/funcalc/

Corecalc and Funcalc
Corecalc is an implementation of core spreadsheet functionality in C#, intended as a platform for experiments with technology and novel functionality. Corecalc is a research prototype, not a usable replacement for Microsoft Excel, Gnumeric or LibreOffice Calc.
Funcalc is an extension of Corecalc in which users can define their own functions via sheet-defined functions, without resorting to external languages such as VBA. These are compiled to .NET bytecode at run-time, thus offering high performance while preserving the usual mode of interaction in which all edits take effect immediately. Funcalc is a research prototype.

The goal of this work is to improve the support for end-user development, which to a large extent takes place within spreadsheets. However, current spreadsheet programs provide little support for abstraction and reuse of computations, unless one uses external languages such as VBA. We want to demonstrate that abstraction and reuse can be supported within the standard spreadsheet metaphor cells, formulas and extreme interactivity, yet with excellent performance.

News
June 2014: My book Spreadsheet Implementation Technology appeared at MIT Press in September 2014. The book's Amazon.com page and its MIT Press page.
June 2014: Jonas Druedahl Rask and Simon Eikeland Timmermann's MSc thesis describing Funsheet, an Excel plugin providing sheet-defined functions through the Funcalc implementation.
Funcalc
A dump of the Funcalc 0.14.0.0 source code as of 27 September 2014, as a Visual Studio 2012 solution. Binaries and examples are in SDFCalc/CoreCalc/bin/Release/. You will need NET 4.5 or later to run Funcalc.exe.
When I find the time, I will further clean up the source code, add more method documentation, and move the source to Github to facilitate sharing and branching.
Here is the Funcalc source code prettyprinted as PDF in dense A4 format.
Corecalc (old, superseded by Funcalc)
The report A Spreadsheet Core Implementation in C# (ITU-TR-2006-91, 135 pages).
The report contains an introduction to the spreadsheet computation model, a survey of spreadsheet literature and implementations, a description of the Corecalc implementation and design alternatives, a detailed presentation of one way to support efficient recalculation, summaries of the work of Iversen's and of Cortes and Hansen, and a list of all known spreadsheet-related patents and patent applications.
Download ITU-TR-2006-91 in PDF
Download ITU-TR-2006-91 in PDF, 2 pages to one
Corecalc code base version 0.7.1 (zip of a VS2008 solution)
Student projects contributing to or using Corecalc and Funcalc
Most recent first. These projects contributed to the development of Corecalc and Funcalc either indirectly by trying out new ideas or pointing out weaknesses, or directly by contributing code. Thanks!
Jonas Druedahl Rask and Simon Eikeland Timmermann: Funsheet. Integration of sheet-defined functions in Excel using C#. MSc thesis, June 2014.
Jens Zeilund Sorensen: An evaluation of sheet defined financial functions in Funcalc. MSc thesis, IT University of Copenhagen, March 2012. Zip file of XMLSS format workbook containing the financial sheet-defined function implementations.
Nader Salas: Collaborative spreadsheet with traceability. MSc thesis, August 2011.
Linas Patapavicius: Efficient linear algebra operations in spreadsheets. MSc thesis, February 2011.
Jens Hammelsvang Hamann: Parallelization of spreadsheet computations. MSc thesis, May 2010.
Tim Garbos and Kasper Videbæk: Spreadsheet optimization on GPUs. BSc thesis, May 2010.
Anders Hartzen: User-defined functions in Excel via ordinary spreadsheet concepts. BSc thesis, May 2010.
Vincens Riber Mink and Daniel Schiermer: Collaborative spreadsheet. TeamCalc, an implemented prototype. BSc thesis, May 2010.
Morten Poulsen and Poul Serek's Master's thesis (PDF): Optimized recalculation for spreadsheets with the use of support graph
Daniel Cortes and Morten Hansen's Master's thesis (PDF): User-defined functions in spreadsheets, 2006.
Thomas Iversen's Master's thesis (PDF): Runtime code generation to speed up spreadsheet computations, 2006.
Authors and credits
The implementation is Copyright 2006-2014 Peter Sestoft and others, and is released under a MIT-style open license.
Peter Sestoft; last update 2017-08-03